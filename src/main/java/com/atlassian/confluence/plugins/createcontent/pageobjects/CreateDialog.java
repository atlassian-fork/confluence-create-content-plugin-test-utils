package com.atlassian.confluence.plugins.createcontent.pageobjects;

import java.util.List;
import java.util.Set;

import com.atlassian.confluence.pageobjects.component.dialog.Dialog;
import com.atlassian.confluence.pageobjects.page.content.CreateBlog;
import com.atlassian.confluence.pageobjects.page.content.CreatePage;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * PageObject for the Blueprint dialog
 */
public class CreateDialog extends Dialog
{
    @ElementBy(className="templates")
    PageElement templateList;

    @ElementBy(className="add-remove-customise-templates-trigger")
    PageElement editTemplatesLink;

    public CreateDialog()
    {
        super("create-dialog");
    }

    @WaitUntil
    public void waitForBlueprints()
    {
        // This CSS selector will be visible when:
        //   a. the dialog is loaded and visible, and
        //   b. the Blueprints have been retrieved by the REST call and populated in the dialog
        // If we disabled the 'Next' button until the dialog was ready, we could wait for that.
        driver.waitUntilElementIsVisible(By.cssSelector(".create-dialog-body .template"));
    }

    private boolean selectItem(String dataKey, String dataValue)
    {
        PageElement template = templateList.find(By.cssSelector("li[" + dataKey + "='" + dataValue + "']"));
        if (template.isPresent()) {
            template.click();
            return true;
        }
        return false;
    }

    public CreateDialog selectContentType(String moduleCompleteKey)
    {
        selectItem("data-item-module-complete-key", moduleCompleteKey);
        return this;
    }

    public CreateDialog selectTemplate(final long templateId)
    {
        selectItem("data-template-id", String.valueOf(templateId));
        return this;
    }

    public CreatePage submitForCreatePageEditor()
    {
        return submit(CreatePage.class);
    }

    public CreateBlog submitForCreateBlogEditor()
    {
        return submit(CreateBlog.class);
    }

    /**
     * Press the dialog's Next button and return a bound Page Object of the type specified.
     */
    public <P> P submit(Class<P> clazz)
    {
        clickButton(id + "-create-button", false);
        return pageBinder.bind(clazz);
    }

    public String getParentTitle()
    {
        return getParentSpan().getText();
    }

    public boolean isParentVisible()
    {
        return getParentSpan().isDisplayed();
    }

    private WebElement getParentSpan()
    {
        return driver.findElement(By.cssSelector("#create-dialog-parent-container span"));
    }

    public boolean hasTemplateItem(String itemModuleCompleteKey)
    {
        String xpath = String.format("//li[@data-item-module-complete-key='%s']", itemModuleCompleteKey);
        return driver.findElements(By.xpath(xpath)).size() > 0;
    }

    public String getWarningMessage()
    {
        List<WebElement> messages = driver.findElements(By.cssSelector("#create-dialog .space-select-control-container .description"));
        if (messages.size() == 0)
            return null;  // no message

        return messages.get(0).getText();
    }

    public void assertThatEditTemplatesLinkIsNotPresent()
    {
        waitUntilFalse(editTemplatesLink.timed().isVisible());
    }

    public void assertThatEditTemplatesLinkIsPresent()
    {
        waitUntilTrue(editTemplatesLink.timed().isVisible());
    }

    public TemplatesPage clickEditTemplatesLink()
    {
        editTemplatesLink.click();
        switchToOtherWindow();
        return pageBinder.bind(TemplatesPage.class);
    }

    /**
     * Switch to whichever other browser window is present, other than the current one.
     * This may well be flaky, and need robustifying.
     */
    private void switchToOtherWindow()
    {
        final String currentWindowHandle = driver.getWindowHandle();
        final Set<String> allWindowHandles = driver.getWindowHandles();
//        System.out.println("Current window handle is [" + currentWindowHandle + "]");
//        System.out.println("Others window handles are " + allWindowHandles);

        assertThat("Expected more than a single browser window to be open", allWindowHandles.size(), is(not(1)));
        for (String otherWindowHandle : allWindowHandles)
        {
            if (!otherWindowHandle.equals(currentWindowHandle))
            {
                driver.switchTo().window(otherWindowHandle);
                return;
            }
        }
    }

    public static class TemplatesPage
    {
        @ElementBy(id="title-text")
        PageElement titleText;

        @ElementBy(cssSelector=".tabs-menu .menu-item.active-tab a")
        PageElement activeTabText;

        public TimedElement titleText()
        {
            return titleText.timed();
        }

        public TimedElement activeTabText()
        {
            return activeTabText.timed();
        }
    }
}
